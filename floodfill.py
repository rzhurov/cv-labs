__author__ = 'zhurov'
import matplotlib.pyplot as plt
import numpy as np
import Image
from skimage.filter import threshold_otsu


def floodfill(bimage):
    labels = np.zeros(bimage.shape, dtype=np.uint8)
    last_label = 0
    collisions = {}

    for i in range(1, bimage.shape[0]):          #          ___
        for j in range(1, bimage.shape[1]):      #       ___|p1|
            if not bimage[i][j]:                 #       |p2| x|
                p1 = labels[i - 1][j]            #               where x is current element
                p2 = labels[i][j - 1]
                if not p1 and not p2:
                    last_label += 1
                    labels[i][j] = last_label

                elif p1 != p2:
                    if not p1 or not p2:
                        labels[i][j] = p1 + p2  # because one of them is a zero and other is a label
                    else:
                        labels[i][j] = p1
                        if p1 not in collisions.keys():  # next six lines are neccesary to correctly process
                            collisions[p1] = p2          # collisions
                        if p1 in collisions.values():
                            for key, val in collisions.iteritems():
                                if val == p1:
                                    collisions[key] = p2
                else:
                    labels[i][j] = p1

    for i in range(1, bimage.shape[0]):
        for j in range(1, bimage.shape[1]):
            if labels[i][j] in collisions.keys():
                labels[i][j] = collisions[labels[i][j]]
    return labels


def main():
    i = Image.open('im6.png').convert('L')
    a = np.array(i)

    global_thresh = threshold_otsu(a)
    binary_image = a > global_thresh

    binary_array = np.array(binary_image, dtype=np.uint8)

    labelled_array = floodfill(binary_array)

    labelled_image = Image.fromarray(labelled_array)

    plt.imshow(labelled_image)
    plt.axis('off')
    plt.show()


if __name__ == "__main__":
    main()